Title: Determining Dependencies of Autotools-based Packages
CSS: /css/main.css

#{include head}

Determining Dependencies of Autotools-based Packages
====================================================

* This will become a table of contents (this text will be scraped).
{:toc}

## Finding what's needed

A sensible starting point when hunting for dependencies is:

    ./configure --help

Pay attention to the various `--{enable,disable}-foo` and `--with{,out}-foo` options in the output,
as they often correspond with dependencies.

Dependencies in `configure.ac`/`configure.in` are usually specified using one of the following
macros:

- `AC_CHECK_LIB`
- `PKG_CHECK_MODULES`
- `GTK_DOC_CHECK` (annoyingly, gtk-doc has its very own macro)

For extra thoroughness, check the following macros (but usually the results are redundant with the
above checks):

- `AC_CHECK_HEADERS`

For even more thoroughness, build the package then check which shared libs are used by its ELF
binaries:

    readelf -d /usr/bin/foo | grep NEEDED
    readelf -d /usr/lib64/libfoo.so | grep NEEDED

There are some caveats for the `readelf` approach:

- It misses dependencies pulled in by the `dlopen()` call

- Libtool may add extra links culled from a binary's true dependencies, and though these are also
  reported as NEEDED, they shouldn't be considered dependencies

`troll.rb` is a convenient `readelf` wrapper which lists dependencies of ELF binaries on a
per-package basis. It can be obtained with:

    git clone git://git.exherbo.org/~ingmar/troll

Usage:

    troll.rb dev-libs/libusb

Results are given like so:

    libc.so.6
    libusb-0.1.so.4
    libstdc++.so.6
    libm.so.6
    libgcc_s.so.1
    librt.so.1
    libpthread.so.0
    libc.so.6

## Things to exclude

Generally, anything that's part of a basic Unixy system, or that's part of the basic C toolchain,
should not be listed in `DEPENDENCIES`, as they are already present on a functional Exherbo system.

--
Copyright 2010 Sterling X. Winter

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
