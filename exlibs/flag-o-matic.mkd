Title: flag-o-matic.exlib
CSS: /css/main.css

#{include head}

# flag-o-matic.exlib

* This will become a table of contents (this text will be scraped).
{:toc}

## Overview

`flag-o-matic.exlib` is an exlib which deals with things related to `$CFLAGS`,
`$CXXFLAGS`, and `$LDFLAGS`.

It includes functions for retrieving, removing, appending, and replacing flags.

## Functions

### `setup-allowed-flags()`

Exports a list of flags which are allowed to be used by the user.

### `_filter-var()`

Internal function to remove occurances of a string in whatever is given as `$1`.

### `filter-flags()`

Filter out all flags passed as arguments from `CFLAGS`, `CPPFLAGS`, and `CXXFLAGS`.

This is often useful for if a package breaks with a certain optimization level.

#### Usage

    pkg_setup() {
        # category/cool-pkg breaks when optimization turned up to 11
        filter-flags -O11
    }

### `filter-ldflags()`

Same as `filter-flags()`, but only operates on `LDFLAGS`.

### `append-flags()`

Append all arguments given to `$CFLAGS` and `$CXXFLAGS`.

#### Usage

    pkg_setup() {
        # we only want to compile with warpspeed enabled
        append-flags -O3
    }

### `append-cppflags()`, `append-ldflags()`

Append all arguments given to `$CPPFLAGS` or `$LDFLAGS`.

#### Usage

    pkg_setup() {
        append-cppflags -D_GNU_SOURCE
        append-ldflags --as-needed
    }

### `replace-flags()`

Replace all occurances of a flag in `$CFLAGS` and `$CXXFLAGS`.

#### Usage

    pkg_setup() {
        # replace all usage of -O2 with -Os
        replace-flags -O2 -Os
    }

### `strip-flags()`, `strip-unsupported-flags`

Remove everything that is not allowed by `setup-allowed-flags()`.

Useful for when the `$CFLAGS` a package wants are very finnicky, and could very
easily break the package.

#### Usage

`strip-flags`

### `test-flag-PROG()`, `test-flag-CC()`, `test-flag-CXX()`, `test-flags-PROG()`, `test-flags-CC()`, `test-flag-CXX()`

Test compiling against the specified compiler variable with flag(s) given as
arguments.

#### Usage

    test-flag-PROG CXX -Os
    test-flag-CC -Os
    test-flag-CXX -Os
    test-flags-PROG CXX -Os -g
    test-flags-CC -Os -g
    test-flags-CXX -Os -g

### `strip-unsupported-flags()`

Test all CFLAGS and CXXFLAGS and set only ones that the compiler supports.

#### Usage

`strip-unsupported-flags`

### `get-flag()`

Get value of a flag in `$CFLAGS` or `$CXXFLAGS`.

`-march` would return `-march=i686`, `march` would return `i686`.

#### Usage

    if [[ $(get-flag mtune) != generic ]];then
        true # something fancy
    fi

### `raw-ldflags()`

Creates raw `LDFLAGS` for usage with the actual `ld` program. Basically,
removing the `-Wl,` from the `LDFLAGS`.

If no argument is specified, `$LDFLAGS` is used as input.

#### Usage

    raw-ldflags "-Wl,-foo -Wl,-bar"
    # returns "-foo -bar"

    raw-ldflags
    # returns $LDFLAGS with all the "-Wl," removed

### `print-flags()`

Print the value of `"$1"_"$2"`. This is used to print a target's `CFLAGS`,
`LDFLAGS`, etc.

In /etc/paludis/bashrc, we require that CFLAGS for cross targets be set in
the format of `${CHOST}_CFLAGS`, `${CHOST}_CXXFLAGS`, and so on. Due to bash
limitations, we can not create variables with dashes in them, and it requires
a lot of indirection to call a variable with a name based on another variable.

#### Usage

    DEFAULT_SRC_COMPILE_PARAMS=(
        CFLAGS=$(print-flags $(exhost --build) CFLAGS)
    )

### `print-build-flags()`

A useful shorthand for `print-flags $(exhost --build)`. Programs that, during
cross compilation, have to compile something to run on the host often need to
have their `CFLAGS` set for the host, and this is what you use to do that.

#### Usage

    DEFAULT_SRC_COMPILE_PARAMS=(
        HOSTCFLAGS=$(print-build-flags CFLAGS)
    )

--
Copyright 2015 Kylie McClain

#{include CC_3.0_Attribution}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
